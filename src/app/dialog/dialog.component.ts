import { Component, OnInit,Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from "@angular/forms";
import { CrudService } from "src/app/service/crud.service";
import { MatDialogRef,MAT_DIALOG_DATA } from "@angular/material/dialog";
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

  personForm:FormGroup;
  submitBtn :string ="Сохранить";
  titleDialog:string ="Создание сотрудника"


  constructor(private formBuilder : FormBuilder,
    @Inject(MAT_DIALOG_DATA) public editData : any,
    private crudService: CrudService,
    private dialogRef:MatDialogRef<any>,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.personForm = this.formBuilder.group({
      firstName : ['',Validators.required],
      lastName : ['',Validators.required]
    });
    
    if (this.editData) {
      this.submitBtn = "Изменить";
      this.titleDialog = "Изменение сотрудника";
      this.personForm.controls['firstName'].setValue(this.editData.firstName);     
      this.personForm.controls['lastName'].setValue(this.editData.lastName);     
    }
  }
   
  addPerson(){
    if(!this.editData){
      if (this.personForm.valid) {
        this.crudService.addPerson(this.personForm.value).subscribe(res=>{
          this.personForm.reset();
          this.dialogRef.close('Сохранить')
          
        }, err => {
          this.snackBar.open("Ошибка добавления сотрудника!");
        })
      }
    }else{
      this.updatePerson();
    }
  }

  updatePerson(){
    this.crudService.editPerson(this.editData.id,this.personForm.value).subscribe(res =>{
      this.personForm.reset();
      this.dialogRef.close('Изменить');
    }, err =>{
      this.snackBar.open('Ошибка изменения сотрудника!')
    })
  }


}
 