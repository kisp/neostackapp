import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/model/person';
import { CrudService } from 'src/app/service/crud.service';
import { FormGroup} from "@angular/forms";
import { MatTableDataSource } from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import { DialogComponent } from 'src/app/dialog/dialog.component';
import { ToolTipComponent } from "src/app/tool-tip/tool-tip.component";
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  template :``
})

export class DashboardComponent implements OnInit {


  constructor(private dialog:MatDialog, private crudService: CrudService,private snackBar: MatSnackBar) { }

  displayedColumns: string[] = ['id','firstName','lastName','action'];
  dataSource:MatTableDataSource<any>;

  formValue : FormGroup;

  ngOnInit(): void {
    this.getAllPerson();
  }

  openDialogDelete(element:any){
    this.dialog.open(ToolTipComponent,{
      data: element   
    }).afterClosed().subscribe(val=>{
      if (val == 'Удалить') {
        this.getAllPerson();
        this.snackBar.open('Сотрудник удален')
      }
    })
  }

  showFormEdit (element:any){
    this.dialog.open(DialogComponent,{
      data:element
    }).afterClosed().subscribe(val=>{
      if (val == 'Изменить') {
        this.getAllPerson();
        this.snackBar.open('Сотрудник изменен')
      }
    })
  }
  

  openDialog() {
    this.dialog.open(DialogComponent, {
    }).afterClosed().subscribe(val=>{
      if (val == "Сохранить") {
        this.getAllPerson();
        this.snackBar.open('Сотрудник добавлен')
      }
    })
  }


  getAllPerson(){
    this.crudService.getAllPerson().subscribe(res => {
      this.dataSource = new MatTableDataSource(res);
    }, err =>{
      this.snackBar.open('Ошибка получения списка сотрудников!')
    })
  }  

}