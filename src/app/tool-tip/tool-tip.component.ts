import { Component, OnInit,Inject } from '@angular/core';
import { CrudService } from "src/app/service/crud.service";
import { MatDialogRef,MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-tool-tip',
  templateUrl: './tool-tip.component.html',
  styleUrls: ['./tool-tip.component.css']
})
export class ToolTipComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public editData : any,
  private crudService: CrudService,
  private dialogRef:MatDialogRef<any>,
  private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  deletePerson(){
    this.crudService.deletePerson(this.editData).subscribe(res =>{
      this.dialogRef.close("Удалить")
    }, err =>{
      this.snackBar.open('Ошибка удаления сотрудника')
    })
  }
}
