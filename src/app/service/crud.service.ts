import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Person } from '../model/person';
@Injectable({
  providedIn: 'root'
})

export class CrudService {
  serviceURL : string ; 

  constructor(private http : HttpClient) {
    this.serviceURL = "http://localhost:3000/persons"
  }
  
  addPerson(person : Person) : Observable<Person> {
    return this.http.post<Person>(this.serviceURL,person);
  }

  getAllPerson() : Observable<Person[]> {
    return this.http.get<Person[]>(this.serviceURL);
  }

  deletePerson(person : Person) : Observable<Person> {
    return this.http.delete<Person>(this.serviceURL+'/'+person.id);
  }

  editPerson(id:any,person:any) : Observable<Person> {
    return this.http.put<any>(this.serviceURL+'/'+id,person);
  }
}
