## NeoStackApp


How to support ie11, I didn't find, so I didn't do it.
I really want to work in your company.

---

To start project, you need install [nodeJs](https://nodejs.org/en/), ***npm*** and ***angular cli*** for do this use that command

```
npm install -g npm
npm install -g @angular/cli
```

Then with button **clone** we can cloning the project on pc and start it with similar command in cmd

```
git clone https://gitlab.com/kisp/neostackapp.git c:\users\user\someName
cd someName
ng serve
```

If you want to start the database, you can do it with the command 

```
npm json-server --watch db.json
```

---


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.




<!-- ## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page. -->
